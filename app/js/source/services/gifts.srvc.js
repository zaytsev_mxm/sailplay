(function () {
  'use strict';

  angular.module('app').service('GiftsSrvc', GiftsSrvc);

  GiftsSrvc.$inject = ['SP_CONST', '$q', '$log'];

  function GiftsSrvc(SP_CONST, $q, $log) {
    var service = {},
      sp = SP_CONST.SP;

    service.GetGiftsList = GetGiftsList;
    service.GetGiftDetails = GetGiftDetails;
    service.PurchaseGift = PurchaseGift;

    return service;

    function GetGiftsList() {
      return $q(function (resolve, reject) {
        sp.send('load.gifts.list');
        sp.on('load.gifts.list.success', function (response) {
          resolve(response);
        });
        sp.on('load.user.info.error', function (response) {
          reject(response);
        });
      });
    }

    function GetGiftDetails(id) {
      id = id || null;

      return $q(function (resolve, reject) {
        sp.send('gifts.get', id);
        sp.on('gifts.get.success', function (response) {
          resolve(response);
        });
        sp.on('gifts.get.error', function (response) {
          reject(response);
        });
      });
    }

    function PurchaseGift(gift) {
      return $q(function (resolve, reject) {
        sp.send('gifts.purchase', {
          gift: gift
        });
        sp.on('gifts.purchase.success', function (response) {
          resolve(response);
        });
        sp.on('gifts.purchase.error', function (response) {
          reject(response);
        });
      });
    }
  }
})();