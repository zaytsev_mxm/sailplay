(function () {
  'use strict';

  angular.module('app').service('UserSrvc', UserSrvc);

  UserSrvc.$inject = ['SP_CONST', '$q', '$log'];

  function UserSrvc(SP_CONST, $q, $log) {
    var service = {},
      sp = SP_CONST.SP;

    service.GetUserDetails = GetUserDetails;

    return service;

    function GetUserDetails() {
      return $q(function (resolve, reject) {
        sp.send('load.user.info');
        sp.on('load.user.info.success', function (response) {
          resolve(response);
        }); 
        sp.on('load.user.info.error', function(response){
          reject(response);
        });
      });
    }
  }
})();