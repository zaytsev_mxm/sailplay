(function () {
  'use strict';

  angular.module('app').service('ActionsSrvc', ActionsSrvc);

  ActionsSrvc.$inject = ['SP_CONST', '$q', '$log'];

  function ActionsSrvc(SP_CONST, $q, $log) {
    var service = {},
      sp = SP_CONST.SP;

    //service.GetActionsList = GetActionsList;

    return service;

    // this methid not yet implemented in API
    function GetActionsList() {
      return $q(function (resolve, reject) {
        sp.on('login.success', function () {
          sp.send('load.actions.list');
          sp.on('load.actions.list.success', function (response) {
            resolve(response);
          });
          sp.on('load.actions.list.error', function (response) {
            reject(response);
          });
        });
      });
    }
  }
})();