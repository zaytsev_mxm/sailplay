(function (sp) {
  'use strict';

  // Define main module
  angular.module('app', ['ui.router']);

  // Define constants
  var SP_CONST = {
    SP: sp,
    PARTNER_ID: 1564,
    AUTH_HASH: 'ac67b582c91e14f189b77eb7abea9302e9efa21d'
  };
  angular.module('app').constant('SP_CONST', SP_CONST);

  // Run app
  function runApp(){
    var sp = SP_CONST.SP;

    sp.send('init', {
      partner_id: SP_CONST.PARTNER_ID
    });

    sp.on('init.success', function () {
      sp.send('login', SP_CONST.AUTH_HASH);
      sp.on('login.success', function () {
        angular.bootstrap(document.body, ['app']);
      });
    });
  }
  
  runApp();

  // Configure routes
  angular.module('app').config(['$stateProvider', '$urlRouterProvider',
     function ($stateProvider, $urlRouterProvider) {
      // For any unmatched url, redirect to /state1
      $urlRouterProvider.otherwise('/');

      // Now set up the states
      $stateProvider
        .state('main', {
          url: '/',
          templateUrl: 'partials/lk.html',
          controller: 'LkCtrl',
          controllerAs: 'vm'
        });
    }]);
})(window.SAILPLAY);