(function(){
  'use strict';
  
  angular.module('app').directive('spGifts', spGifts);
  
  spGifts.$inject = [];
  
  function spGifts(){
    return {
      restrict: 'E',
      replace: true,
      templateUrl: 'partials/gifts.html'
    }
  }
})();