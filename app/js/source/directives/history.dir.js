(function(){
  'use strict';
  
  angular.module('app').directive('spHistory', spHistory);
  
  spHistory.$inject = [];
  
  function spHistory(){
    return {
      restrict: 'E',
      replace: true,
      templateUrl: 'partials/history.html'
    }
  }
})();