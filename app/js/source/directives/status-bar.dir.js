(function(){
  'use strict';
  
  angular.module('app').directive('spStatusBar', spStatusBar);
  
  spStatusBar.$inject = [];
  
  function spStatusBar(){
    return {
      restrict: 'E',
      replace: true,
      templateUrl: 'partials/status-bar.html'
    }
  }
})();