(function(){
  'use strict';
  
  angular.module('app').directive('spGift', spGift);
  
  spGift.$inject = ['$log'];
  
  function spGift($log){
    return {
      restrict: 'E',
      replace: true,
      templateUrl: 'partials/gift.html',
      scope: {
        gift: '=',
        purchaseFn: '='
      },
      link: function($scope, $elem, attrs){
        var btnElem = $elem.find('a'),
            disableBtn = function(){
              btnElem.attr('disabled', 'disabled');
            },
            enableBtn = function(){
              setTimeout(function(){
                btnElem.removeAttr('disabled');
              },500);
            };
        
        btnElem.bind('click', function(){
          disableBtn();
          
          $scope.purchaseFn($scope.gift).then(function(response){
            enableBtn();
            
            alert('Поздравляем!');
          }).catch(function(response){
            enableBtn();
            
            switch (response.reason || null) {
              case 'notEnoughPoints': {
                alert('Недостаточно очков для этого подарка!');
              } break;
              default: {
                alert('Что-то пошло не так');
              } break;
            }
          });
        });
      }
    }
  }
})();