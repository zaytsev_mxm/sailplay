(function(){
  'use strict';
  
  angular.module('app').directive('spTasks', spTasks);
  
  spTasks.$inject = [];
  
  function spTasks(){
    return {
      restrict: 'E',
      replace: true,
      templateUrl: 'partials/tasks.html'
    }
  }
})();