(function () {
  'use strict';

  angular.module('app').controller('LkCtrl', LkCtrl);

  LkCtrl.$inject = ['UserSrvc', 'GiftsSrvc', '$q', '$timeout', '$log'];

  function LkCtrl(UserSrvc, GiftsSrvc, $q, $timeout, $log) {
    var vm = this;

    vm.user = null;
    vm.user_points = null;
    vm.available_gifts = null;
    
    vm.purchaseGift = purchaseGift;
    
    vm.max_points = 300;

    loadUserDetails();
    loadGiftsList();

    function loadUserDetails() {
      UserSrvc.GetUserDetails().then(function (response) {
        $log.log('User data: ', response);
        
        if (vm.user == null) {
          angular.element(document.body).addClass('data-loaded');
        }

        vm.user = response.user;
        
        $log.log(response.user_points);
        
        // timeout for a buffer for the progress bar animation
        $timeout(function(){
          vm.user_points = response.user_points;
        },300);
      }).catch(function (response) {
        $log.log('Failed to load user details: ', response);
      });
    }

    function loadGiftsList() {
      GiftsSrvc.GetGiftsList().then(function(response){
        $log.log('User gifts: ', response);
        
        vm.available_gifts = response;
      }).catch(function(response){
        $log.log('Failed to load user gifts: ', response);
      });
    }
    
    function purchaseGift(gift){
      return $q(function(resolve, reject){
        if (gift.points > vm.user_points.confirmed) {
          reject({
            reason: 'notEnoughPoints'
          });
        } else {
          GiftsSrvc.PurchaseGift(gift).then(function(response){
            $log.log('Gift #' + gift.id + ' successfully purchased!');

            loadUserDetails();
            resolve(response);
          }).catch(function(response){
            $log.log('Failed to purchase gift #' + gift.id);
            
            reject({
              reason: 'networkError'
            });
          });
        }
      });
    }
  }
})();