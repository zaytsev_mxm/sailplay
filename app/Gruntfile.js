module.exports = function(grunt) {
    require('jit-grunt')(grunt);

    grunt.initConfig({
        concat: {
            scripts: {
                options: {
                    separator: ';'
                },
                src: ['js/source/**/*.js'],
                dest: 'js/app.js'
            }
        },
        uglify: {
            scripts: {
                files: {
                    'js/app.min.js': ['js/app.js']
                }
            }
        },
        compress: {
            scripts: {
                options: {
                    mode: 'gzip'
                },
                files: [{expand: true, src: ['js/app.min.js'], ext: '.min.gz.js'}]
            }
        },
        watch: {
            scripts: {
                files: ['js/source/**/*.js'],
                tasks: ['concat:scripts', 'uglify:scripts', 'compress:scripts'],
                options: {
                    nospawn: true
                }
            }
        }
    });

    grunt.registerTask('default', ['concat', 'uglify', 'compress', 'watch']);
};